#!/bin/bash

curl -s https://api.github.com/repos/docker/compose/releases | 
    python -c 'import sys, json; assets = [release["assets"] for release in json.load(sys.stdin) if not release["prerelease"]]; urls = [asset["browser_download_url"] for asset in assets[0] if asset["name"] == "docker-compose-Linux-x86_64"]; print(urls[0]);'

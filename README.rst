ansible-role-swarm
==================

.. _`Ansible`:
.. _`ansible-role-debian-apt`:
.. _`Docker Engine`:
.. _`Docker Swarm`:

This project contains Ansible roles that installs `Docker Engine`_ and runs it
in `Docker Swarm`_ mode.

Supported distributions
-----------------------

* Debian GNU/Linux

Requirements
------------

* `Ansible 2`_

  + Ansible must be configured to merge dicts, e.g. ``hash_behaviour = merge``
  must be present in your ``ansible.cfg``.
* `ansible-role-debian-apt`_

Installation
------------

There are two possibilities how to install the roles. Choose one that suits you best.

#. Copy the role into the ``roles`` subdirectory: ::

    cd YOUR_PROJECT
    mkdir -p roles/swarm
    wget -O - \
        https://gitlab.com/tomaskadlec/ansible-role-swarm/repository/archive.tar.bz2?ref=master \
        | tar xjf - --wildcards -C roles/swarm --strip=1

#. If your project uses git you can make use of git submodules: ::

     cd YOUR_PROJECT
     mkdir -p roles
     git submodule add git@gitlab.com:tomaskadlec/ansible-role-swarm.git roles/swarm

Configuration
-------------

.. _`docker-repo/defaults/main.yml`: docker-repo/defaults/main.yml
.. _`swarm/defaults/main.yml`: swarm/defaults/main.yml

The ``swarm/docker-repo`` holds configuration of apt repository with
``docker-engine`` package. This configuration is used by
`ansible-role-debian-apt`_ so this role must run first. Please refer to 
`docker-repo/defaults/main.yml`_ for the configuration.

The ``swarm/docker`` role is responsible for installing `Docker Engine`_ from
aforementioned repository. This role depends on ``swarm/docker-repo`` role.

The ``swarm/swarm`` role initializes a Docker Swarm cluster. It has just one
variable that holds a list of clusters in the project.

``clusters``
    A list of clusters in the project - each cluster must have at least one host
    in group ``masters``. Such host will be elected to start the cluster.
    Defaults to empty list.

Usage
-----

Please refer to the following example for usage: ::

    -   name: "Install or update Docker Engine"
        hosts: [all]
        tags: [docker]
        roles:
            -   swarm/docker-repo
            -   debian-apt
            -   swarm/docker

    -   name: "Setup Docker Swarm"
        hosts: [all]
        tags: [swarm]
        roles:
            -   swarm/swarm

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
